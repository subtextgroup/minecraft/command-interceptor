package com.sg.mc.ci;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Ryan on 4/9/2017.
 */
public class CommandInterceptor extends JavaPlugin implements Listener, CommandExecutor {

    Map<String, Interceptor> interceptors;

    @Override
    public void onEnable() {
        ConfigurationSerialization.registerClass(Interceptor.class);
        createConfig();
        loadConfig();

        getCommand("addinterceptor").setExecutor(this);

        getServer().getPluginManager().registerEvents(this, this);
        super.onEnable();
    }



    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void intercept(PlayerCommandPreprocessEvent e) {
        String original = e.getMessage().substring(1).split(" ")[0].toLowerCase();
        Interceptor i = interceptors.get(original);
        if(i != null) {
            if(!e.getPlayer().isPermissionSet(i.getPermission())) {
                e.setCancelled(true);
                String rejectMessage = i.getRejectMessage();
                if(rejectMessage == null) {
                    rejectMessage = "You are not authorized to execute the command \"" + original + "\"";
                }
                e.getPlayer().sendMessage(rejectMessage);
                if(i.isReplace()) {
                    String commandLine = i.getReplacement();
                    if("~".equals(i.getIncludeArgs()) && e.getMessage().trim().indexOf(" ") > -1) {
                        String[] originalArgs = e.getMessage().split(" ");
                        String[] args = Arrays.copyOfRange(originalArgs, 1, originalArgs.length);
                        commandLine += " " + Arrays.stream(args).collect(Collectors.joining(" "));
                    } else if(!"^".equals(i.getIncludeArgs())) {
                        commandLine += " " + i.getIncludeArgs().replace(",", " ");
                    }
                    e.getPlayer().sendMessage("Your command has been replaced with: " + commandLine);
                    getServer().dispatchCommand(e.getPlayer(), commandLine);
                }
            }
        }
    }


    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        if(!commandSender.isPermissionSet("ci.addinterceptor")) {
            commandSender.sendMessage("You do not have the ci.addinterceptor permission, which is required to issue this command.");
            return false;
        }
        ///addinterceptor <command> <permission> <replace (true/false)> <includeArgs (true/false)> <replacement> <rejectMessage>

        if(args.length < 5) {
            return false;
        }
        String rejectMessage = null;
        if(args.length > 5) {
            String[] rejectMessageArgs = Arrays.copyOfRange(args, 5, args.length);
            rejectMessage = Arrays.stream(rejectMessageArgs).collect(Collectors.joining(" "));
        }
        Interceptor i = new Interceptor();
        i.setOriginal(args[0].toLowerCase());
        i.setPermission(args[1].toLowerCase());
        i.setReplace(Boolean.parseBoolean(args[2]));
        i.setIncludeArgs(args[3]);
        i.setReplacement(args[4]);
        i.setRejectMessage(rejectMessage);
        List<Interceptor> interceptors = (List<Interceptor>)getConfig().getList("interceptors", new ArrayList<>());
        interceptors.add(i);
        getConfig().set("interceptors", interceptors);
        saveConfig();
        loadConfig();
        return true;
    }



    private void loadConfig() {
        List<Interceptor> saved = (List<Interceptor>)getConfig().getList("interceptors", new ArrayList<>());
        interceptors = new HashMap<>();
        saved.forEach(i -> interceptors.put(i.getOriginal(), i));
    }

    private Configuration createConfig() {
        try {
            if (!getDataFolder().exists()) {
                getDataFolder().mkdirs();
            }
            File file = new File(getDataFolder(), "config.yml");
            if (!file.exists()) {
                getLogger().info("config.yml not found, creating!");
                saveDefaultConfig();
            } else {
                getLogger().info("config.yml found, loading!");
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return getConfig();
    }
}
