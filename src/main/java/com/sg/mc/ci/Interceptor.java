package com.sg.mc.ci;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ryan on 4/9/2017.
 */
public class Interceptor implements ConfigurationSerializable {
    private String original;
    private String permission;
    private boolean replace;
    private String includeArgs;
    private String replacement;
    private String rejectMessage;



    public Interceptor() {

    }
    public Interceptor(Map<String, Object> fields) {
        this.original = (String)fields.get("original");
        this.permission = (String)fields.get("permission");
        this.replace = (boolean)fields.get("replace");
        this.includeArgs = (String)fields.get("includeArgs");
        this.replacement = (String)fields.get("replacement");
        this.rejectMessage = (String)fields.get("rejectMessage");
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public boolean isReplace() {
        return replace;
    }

    public void setReplace(boolean replace) {
        this.replace = replace;
    }

    public String getIncludeArgs() {
        return includeArgs;
    }

    public void setIncludeArgs(String includeArgs) {
        this.includeArgs = includeArgs;
    }

    public String getReplacement() {
        return replacement;
    }

    public void setReplacement(String replacement) {
        this.replacement = replacement;
    }

    public String getRejectMessage() {
        return rejectMessage;
    }

    public void setRejectMessage(String rejectMessage) {
        this.rejectMessage = rejectMessage;
    }

    /**
     * Creates a Map representation of this class.
     * <p>
     * This class must provide a method to restore this class, as defined in
     * the {@link ConfigurationSerializable} interface javadocs.
     *
     * @return Map containing the current state of this class
     */
    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> result = new HashMap<>();
        result.put("original", original);
        result.put("permission", permission);
        result.put("replace", replace);
        result.put("includeArgs", includeArgs);
        result.put("replacement", replacement);
        result.put("rejectMessage", rejectMessage);
        return result;
    }
}
